import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import App from './../App';
import Question from '../components/Question';
import Home from '../components/Home';
import Quiz from './../components/Quiz';
import Response from './../components/Response';


const routes = (
        <Router>
            <App>
                <Switch>
                <Route exact path="/" component={Home} />
                    <Route path='/question' component={Question} />
                    <Route path='/quiz' component={Quiz} />
                    <Route path='/responseQuiz' component={Response} />
                </Switch>
            </App>
        </Router>
);


export default routes;
