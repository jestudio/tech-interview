export const LIST_QUESTION = "LIST_QUESTION";
export const ADD_QUESTION = "ADD_QUESTION";
export const REMOVE_QUESTION = "REMOVE_QUESTION";
export const SET_QUESTION = "SET_QUESTION";