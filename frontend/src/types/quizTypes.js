export const ADD_QUIZ = "ADD_QUIZ";
export const LOAD_QUIZS = "LOAD_QUIZS";
export const REMOVE_QUIZ = "REMOVE_QUIZ";
export const ADD_RESPONSE = "ADD_RESPONSE";
export const LOAD_RESPONSES = "LOAD_RESPONSES";