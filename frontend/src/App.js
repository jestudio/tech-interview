import React, { Component } from 'react';
import './assets/styles/App.css';
import { Layout } from 'antd';
import Headers from './components/Headers';
import Footers from './components/Footers';

const { Content } = Layout;

class App extends Component {
  render() {
    return (
        <Layout className="layout">
          <Headers />
          <Content style={{ padding: '0 50px' }}>
              <div style={{ padding: 24, minHeight: 380 }}>
                {this.props.children} 
              </div>
          </Content>        
          <Footers />
        </Layout>
    );
  }
}

export default App;
