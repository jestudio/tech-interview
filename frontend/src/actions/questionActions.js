import { ADD_QUESTION, LIST_QUESTION, REMOVE_QUESTION } from "../types/questionTypes";

const setNewQuestion = question =>({ type: ADD_QUESTION, payload: question })

export const addQuestion = question =>{
    return dispatch =>{
        dispatch(setNewQuestion(question))
    }
}

export const removeQuestion = questionRemove =>{
    return (dispatch, getState) =>{
        let { questionReducer: { questions } } = getState();
        questions = questions.filter( question => question.id !== questionRemove.id)
        dispatch({ type: REMOVE_QUESTION, payload: questions })
    }

}

export const findQuestions = text =>{
    return dispatch =>{
        dispatch({ type: LIST_QUESTION, payload: text })
    }
}

export const loadQuestions = ()=>{
    return dispatch =>{
        dispatch({ type: LIST_QUESTION })
    }
}




