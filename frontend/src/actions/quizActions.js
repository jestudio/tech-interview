
import { ADD_QUIZ, LOAD_QUIZS, REMOVE_QUIZ, ADD_RESPONSE, LOAD_RESPONSES } from './../types/quizTypes';

export const loadResponses = () =>{
    return dispatch =>{
        dispatch({type: LOAD_RESPONSES })
    }
}

export const addResponseQuiz = response =>{
    return dispatch =>{
        dispatch({type: ADD_RESPONSE, payload: response})
    }
}

export const loadQuizs = () =>{
    return dispatch =>{
        dispatch({type: LOAD_QUIZS })
    }
}

export const removeQuiz = quizRemove =>{
    return (dispatch, getState) =>{
        let { quizReducer: { quizs }  } = getState();
        quizs = quizs.filter( quiz => quiz.id !== quizRemove.id)
        dispatch({ type: REMOVE_QUIZ, payload: quizs })
    }

}

export const addQuiz = quiz =>{
    return dispatch =>{
        dispatch({type: ADD_QUIZ, payload: quiz})
    }
}