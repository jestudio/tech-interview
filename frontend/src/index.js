import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import routes from './config/router';
import store from './config/store';
import { Provider } from 'react-redux';


ReactDOM.render(
    <Provider store={ store }>
        <BrowserRouter>
            { routes }
        </BrowserRouter>
    </Provider>
    , document.getElementById('root'));

