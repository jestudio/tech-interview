
import { ADD_QUIZ, LOAD_QUIZS, REMOVE_QUIZ, ADD_RESPONSE, LOAD_RESPONSES } from './../types/quizTypes';

const INITIAL_STATE = {
    quizs: [],
    responses: []
}

const quizReducer = (state=INITIAL_STATE, action) =>{

    switch(action.type){

        case LOAD_RESPONSES: 
            let cacheResponses = []
            if(localStorage.getItem('responses')){
                cacheResponses = JSON.parse(localStorage.getItem('responses'));
            }

            return {
                ...state,
                responses: cacheResponses
            }

        case ADD_RESPONSE:
            let { responses } = state
            responses.push(action.payload);
            localStorage.setItem('responses', JSON.stringify(responses))
            return{
                ...state,
                responses
            }
        case REMOVE_QUIZ:
            localStorage.setItem("quizs", JSON.stringify(action.payload))
            return{
                ...state,
                quizs: action.payload
            } 

        case LOAD_QUIZS:
            let cacheQuizs = []
            if(localStorage.getItem('quizs')){
                cacheQuizs = JSON.parse(localStorage.getItem('quizs'));
            }

            return {
                ...state,
                quizs: cacheQuizs
            }
        case ADD_QUIZ:
            let { quizs } = state;
            quizs.push(action.payload)
            localStorage.setItem("quizs", JSON.stringify(quizs))
            return {
                ...state,
                quizs
            }
        default:
            return state;
    }

}


export default quizReducer;