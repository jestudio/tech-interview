import { combineReducers } from 'redux';
import questionReducer from './questionReducer';
import quizReducer from './quizReducer';

const rootReducer = combineReducers({
    questionReducer,
    quizReducer
});

export default rootReducer;
