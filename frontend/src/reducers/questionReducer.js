import { ADD_QUESTION, LIST_QUESTION, REMOVE_QUESTION } from "../types/questionTypes";

const INITIAL_STATE = {
    questions: []
}

const questionReducer = (state = INITIAL_STATE, action) =>{
    switch(action.type){

        case REMOVE_QUESTION:
            localStorage.setItem("questions", JSON.stringify(action.payload))
            return {
                ...state,
                questions: action.payload
            }

        case LIST_QUESTION:
            let cacheQuestion = [];
            if(localStorage.getItem('questions')){
                cacheQuestion = JSON.parse(localStorage.getItem('questions'));
            }

            if(action.payload){
                cacheQuestion = cacheQuestion.filter( question =>  question.description.toLowerCase().indexOf(action.payload.toLowerCase()) >= 0 )
            }

            return {
                ...state,
                questions: cacheQuestion
            }

        case ADD_QUESTION:
            let { questions } = state;
            questions.push(action.payload);
            localStorage.setItem("questions", JSON.stringify(questions))
            return {
                ...state,
                questions
            }
        default:
        return state;
    }
}

export default questionReducer;