import React from 'react';
import { connect } from 'react-redux';
import uuidv1 from 'uuid/v1';
import { Card, Button, Table, Tag, Form, Input, Row, Col, Checkbox, Modal, Popconfirm, notification } from 'antd';
import { addQuestion, loadQuestions, removeQuestion, findQuestions } from './../actions/questionActions';


const { info } = Modal;
const FormItem = Form.Item;

class Question extends React.Component{

    state = {
        inserting: false,
        answerDescription: null,
        question: {
            description: null,
            answers: [],
            correct: false
        },
        filter: null
    }

    render(){

        const extraButton =  <Button type="primary" icon="plus" onClick={ this.handleAddQuestion }>New question</Button>
        const actionFilter = [<Button type="default" icon="delete" disabled={ this.state.filter == null } onClick={ this.handleClear }>Clear</Button>]
        const actionInsert = [
                <Button.Group>
                    <Button disabled={ this.state.question.answers.length < 4 || !this.hasCorrectAnswer() } type="primary" icon="save" onClick={ this.handleSave }>Save</Button>
                    <Button type="default" icon="rollback" onClick={ this.handleBack }>Back</Button>
                </Button.Group>
            ]

        const columns = [
            {
                title: "Question",
                dataIndex: "description",
                key: "description"
            },
            {
                title: "Answers",
                dataIndex: "answers",
                key: "answers",
                render: answers =>{
                    return answers.map( answer => <Tag color={ answer.correct ? "#2db7f5" : null } style={{ marginBottom: 5 }} key={ answer.uuid }>{ answer.description }</Tag> )
                }
            },
            {
                title: "Actions",
                render: record =>{
                    return (
                        <div style={ { whiteSpace: "nowrap" } }>
                            <Popconfirm title="Remove this question?" okText="Yes!" onConfirm={()=> this.handleRemoveQuestion(record)}>
                                <Button style={{marginLeft: 10}} type="danger" shape="circle" icon="delete" />
                            </Popconfirm>

                        </div>
                    )
                }
            }

        ]

        const columnsAnswer = [
            {
                title: "Description",
                dataIndex: "description",
                key: "description"
            },

            {
                title: "Correct?",
                dataIndex: "correct",
                key: "correct",
                render: correct => {
                    return correct ? "Yes" : "No"
                }
            },
            {
                title: "Actions",
                render: record =>{
                    return <Button type="danger" shape="circle" icon="delete" onClick={()=> this.handleRemoveAnswer(record) } />
                }
            }
        ]

        return(
            <div>
                { !this.state.inserting && 
                    <div>
                        <Card title="Filter Questions" actions={ actionFilter }>
                            <label>Question:</label><br />
                            <Input placeholder="Filter for question" value={this.state.filter} onChange={ this.handleFilter }  />
                        </Card>

                        <Card title="Questions" extra={ extraButton } style={{ marginTop: 20 }}>
                            <Table rowKey={ item => item.id } dataSource={this.props.questions} columns={ columns } ></Table>
                        </Card>
                    </div>
                }

                {this.state.inserting &&
                    <Card title="New question" actions={ actionInsert }>
                        <Form>
                            <FormItem label="Question">
                                <Input value={this.state.question.description} onChange={ this.handleDescription } />
                            </FormItem>
                            
                            <FormItem label="Answer">
                                <Row  type="flex" gutter={16}>
                                    <Col span={6}>
                                        <Input value={ this.state.answerDescription } onChange={ this.handleAnswerDescription } />    
                                    </Col>
                                    <Col span={3}>
                                        <Checkbox disabled={this.hasCorrectAnswer()} checked={ this.state.question.correct } onChange={ this.handleCorrect }>Is it correct?</Checkbox>
                                    </Col>
                                    <Col span={1}>
                                        <Button disabled={ this.state.answerDescription == null } type="primary" icon="plus" onClick={ this.handleAddAnswer }>Add answer</Button>
                                    </Col>
                                </Row>
                            </FormItem>

                            <Table rowKey={ item => item.uuid } dataSource={this.state.question.answers} columns={ columnsAnswer }></Table>

                        </Form>
                    </Card>
                }

            </div>
        )
    }

    componentDidMount(){
        this.props.loadQuestions();
    }

    handleSave = ()=>{
        let { question } = this.state;

        // UUID for an identified one and thus manipulate the records in the list.
        question.id = uuidv1();
        this.props.addQuestion(question);
        info({
            title: "Success insert",
            content: "Your question has be saved!"
        });
        this.setState({...this.state, inserting: false , question: { description: null, answers: [], correct: false }});
    }

    hasCorrectAnswer = ()=>{
        let { question: { answers } } = this.state;
        return answers.filter( answer => answer.correct ).length > 0;
    }

    handleRemoveAnswer = answerRemove =>{
        let { question: { answers } } = this.state;

        //I always make the list filter, it is more readable and more secure.
        answers = answers.filter( answer => answer.uuid !== answerRemove.uuid );
        this.setState({...this.state, question: {...this.state.question, answers } });
    }

    handleAddAnswer = ()=>{
        let { question: { answers } } = this.state;
        answers.push({uuid: uuidv1(), description: this.state.answerDescription, correct: this.state.question.correct })
        this.setState({...this.state, answerDescription: null , question: {...this.state.question, answers, correct: false } });
    }

    handleAnswerDescription = event =>{
        this.setState({...this.state, answerDescription: event.target.value});
    }

    handleCorrect = () =>{
        this.setState({...this.state, question: {...this.state.question, correct: !this.state.question.correct } });
    }

    handleDescription = event =>{
        this.setState({...this.state, question: {...this.state.question, description: event.target.value } });
    }
    
    handleAddQuestion = () =>{
        this.setState({...this.state, inserting: true});
    }

    handleBack = ()=>{
        this.setState({...this.state, inserting: false});
    }

    handleRemoveQuestion = question =>{
        this.props.removeQuestion(question);
        notification['success']({
            message: "Question removed!",
            description: "The question has been successfully removed!"
        })
    }

    handleFilter = event =>{
        this.props.findQuestions(event.target.value);
        this.setState({...this.state, filter: event.target.value });
    }

    handleClear = ()=>{
        this.props.findQuestions(null);
        this.setState({...this.state, filter: null})
    }
}

const mapStateToProps = state =>({
    questions: state.questionReducer.questions
});

const mapDispatchToProps = dispatch =>({
    addQuestion: question => dispatch(addQuestion(question)),
    loadQuestions: ()=> dispatch(loadQuestions()),
    removeQuestion: question => dispatch(removeQuestion(question)),
    findQuestions: description => dispatch(findQuestions(description))
});

export default connect(mapStateToProps, mapDispatchToProps)(Question);

