import React, { Component } from 'react'
import { Layout } from 'antd';
const { Footer } = Layout;

class Footers extends Component{
    render(){
        return (
            <Footer>
                ©2018 - Jefferson Oliveira
            </Footer>
        )
    }
}

export default Footers;