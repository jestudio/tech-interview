import React, { Component } from 'react'
import { Layout, Menu } from 'antd';
import { Link } from 'react-router-dom';
const { Header } = Layout;

class Headers extends Component{
    
    render(){
        
        let defaultKey;
        switch(window.location.pathname){
            case "/responseQuiz":
                defaultKey = "4";
                break;
            case "/quiz":
                defaultKey = "3";
                break;
            case "/question":
                defaultKey = "2";
                break;
            default:
                defaultKey = "1"
        }

        return (
            <Header>
                <div className="logo" />
                <Menu
                mode="horizontal"
                theme="dark"
                defaultSelectedKeys={[defaultKey]}
                style={{ lineHeight: '64px' }}
                >
                    <Menu.Item key="1" >
                        <Link to={"/"}>Home</Link>
                    </Menu.Item>
                    <Menu.Item key="2" >
                        <Link to={"/question"}>Questions</Link>
                    </Menu.Item>
                    <Menu.Item key="3">
                        <Link to={"/quiz"}>Quiz</Link>
                    </Menu.Item>

                    <Menu.Item key="4">
                        <Link to={"/responseQuiz"}>Response Quizs</Link>
                    </Menu.Item>

                </Menu>
            </Header>
        )
    }
}

export default Headers;