import React from 'react';
import { Card, Table, Button, Steps, Modal } from 'antd';
import { connect } from 'react-redux';
import { loadQuizs, loadResponses } from './../actions/quizActions';
import ResponseQuestion from './ResponseQuestion';

const Step = Steps.Step;
const { info } = Modal;

class Response extends React.Component{

    state = {
        responding: false,
        current: 0,
        currentQuestion: null,
        quiz: { description : null }
    }

    render(){
        const { responses } = this.props;
        const columns = [
            {
                title: "Quiz",
                dataIndex: "description",
                key: "description"
            },
            {
                title: "Response",
                render: record =>{
                    return <Button icon="edit" type="primary" onClick={()=> this.handleResponse(record) }>Response</Button>
                }
            }
        ]

        let actions = [];

        if(this.state.responding){
            actions = [
                <div>
                    <Button.Group>
                        {
                            this.state.current < this.state.quiz.questions.length - 1
                            && <Button type="primary" onClick={() => this.nextQuestion()}>Next</Button>
                        }
                        {
                            this.state.current === this.state.quiz.questions.length - 1
                            && <Button type="primary" onClick={() => this.handleDone()}>Done</Button>
                        }
                        {
                            this.state.current > 0
                            && (
                            <Button onClick={() => this.prevQuestion()}>
                            Previous
                            </Button>
                            )
                        }
                    </Button.Group>
                     <Button type="default" style={{ marginLeft: 10, marginTop: 4 }} icon="rollback" onClick={() => this.handleBack()}>Back</Button>
                </div>
            ]
        }

        return(
            <div>
                { !this.state.responding &&
                    <Card title="Response Quiz">
                        <Table rowKey={ item => item.id } columns={ columns } dataSource={ this.props.quizs } />
                    </Card>
                }

                { this.state.responding &&

                    <Card title={`Quiz: ${ this.state.quiz.description }`} actions={ actions }>

                        <Steps current={ this.state.current } >
                            { this.state.quiz.questions.map( (question, index ) => {
                                        let response = responses.find( response => response.idQuiz === this.state.quiz.id && response.question === question.id  )
                                        let status = "";
                                        if(response !== undefined){
                                            status = response.correct ? "finish" : "error"
                                        }

                                        return <Step status={ status } key={ index } title={ question.description } />
                                    }
                                    
                                )
                            }
                        </Steps>

                        <div>
                            
                        { this.state.quiz.questions.map( (question, index ) => {
                                return index === this.state.current && 
                                <div style={{ marginTop: 15 }} key={ index }>
                                <h3>{ question.description }</h3>
                                <ResponseQuestion  quiz={ this.state.quiz.id } question={ question }  />    
                                </div>
                         })
                        }

                        </div>

                    </Card>
                }
            </div>
        )
    }

    componentDidMount(){
        this.props.loadQuizs();
        this.props.loadResponses();
    }

    handleResponse = quiz =>{

        this.setState({...this.state, responding: true, quiz})
    }

    handleDone = ()=>{
        info({
            title: "Completed!",
            content: "You have completed the quiz! Thank you!"
        })
        this.setState({...this.state, current: 0, responding: false, quiz: { description: null  } })
    }

    handleBack = ()=>{
        this.setState({...this.state, current: 0, responding: false, quiz: { description: null  } })
    }

    nextQuestion = () => {
        const current = this.state.current + 1;
        this.setState({...this.state , current });
      }
    
    prevQuestion = () => {
        const current = this.state.current - 1;
        this.setState({...this.state, current });
    }
}


const mapStateToProps = state =>({
    quizs: state.quizReducer.quizs,
    responses: state.quizReducer.responses
});

const mapDispatchToProps = dispatch =>({
    loadResponses:() =>dispatch(loadResponses()),
    loadQuizs: () => dispatch(loadQuizs())
});

export default connect(mapStateToProps, mapDispatchToProps)(Response);