import React from 'react';
import { Radio, Alert } from 'antd';
import uuidv1 from 'uuid/v1';
import { connect } from 'react-redux';
import { addResponseQuiz } from './../actions/quizActions';

const RadioGroup = Radio.Group;

class ResponseQuestion extends React.Component{

    state = {
        selectedAnswer: null,
        disabled: false,
        response: null
    }

    render() {
        let { question } = this.props;
        return (
            <div style={{ marginTop: 15 }} >
                <RadioGroup disabled={ this.state.disabled } onChange={ this.onChange } value={ this.state.selectedAnswer }>
                    { question.answers.map( (answer, index ) =>
                        <Radio key={ index } value={ answer.uuid }>{ answer.description }</Radio>
                    )}
                </RadioGroup>

                <div style={{ marginTop: 15}}>
                    { this.handleCorrect() }
                </div>
                

            </div>
        )
    }

    handleCorrect = ()=>{
        let alert;
        if(this.state.response != null){
            if(this.state.response.correct ){
                alert = <Alert message="Answer correct!" type="success" showIcon  />
            }else{
                alert = <Alert message="Answer wrong! :(" type="error" showIcon />
            }
        }

        return alert
    }

    componentDidMount(){
        let { question, quiz, responses } = this.props;
        let response = responses.find( response => response.idQuiz === quiz && response.question === question.id );
        if(response){
            this.setState({ selectedAnswer: response.answer, disabled: true, response })
        }
    }

    onChange = event => {
        let { question, quiz } = this.props;
        let answer = question.answers.find( answer => answer.uuid === event.target.value) ;
        let response = {id: uuidv1(), idQuiz: quiz, question: question.id, answer: event.target.value, correct: answer.correct} ;
        
        this.props.addResponseQuiz(response);

        this.setState({
          selectedAnswer: event.target.value,
          disabled: true,
          response
        });
      }
}


const mapStateToProps = state =>({
    responses: state.quizReducer.responses
});

const mapDispatchToProps = dispatch =>({
    addResponseQuiz: response => dispatch(addResponseQuiz(response))
});

export default connect(mapStateToProps, mapDispatchToProps)(ResponseQuestion);

