import React, { Component } from 'react'
import { connect } from 'react-redux';
import uuidv1 from 'uuid/v1';
import { Card, Table, Button, Form, Input, AutoComplete, Popconfirm, Modal, notification, Popover, Badge } from 'antd';
import { loadQuestions } from '../actions/questionActions';
import { addQuiz, loadQuizs, removeQuiz } from './../actions/quizActions';

const { info } = Modal;

class Quiz extends Component{

    state = {
        inserting: false,
        saving: false,
        description: null,
        questions:[],
        dataSourceQuestion: []
    }

    render(){
        const columns = [
            {
                title: "Quiz description",
                dataIndex: "description",
                key: "description"
            },
            {
                title: "Question",
                render: record =>{
                        const questions = record.questions.map( question => <li key={ question.id }>{ question.description }</li> )
                    return (
                        <Popover title="Questions" content={<ul>{ questions }</ul>}>
                            <Badge count={ record.questions.length }>
                                <Button icon="search" shape="circle"></Button>
                            </Badge>
                        </Popover>
                    )
                }
            },
            {
                title: "Actions",
                render: record => {
                    return (
                        <Popconfirm title="Are sure?" onConfirm={()=> this.handleRemoveQuiz(record) }>
                            <Button type="danger" icon="delete" shape="circle"></Button>
                        </Popconfirm>
                    )
                }

            }
        ]


        const extraButton =  <Button type="primary" icon="plus" onClick={ this.handleNewQuiz }>New quiz</Button>
        const actionInsert = [
            <Button.Group>
                <Button type="primary" icon="save" onClick={ this.handleSaveQuiz } disabled={this.state.saving || this.state.questions.length === 0 || (this.state.description === "" || this.state.description===null) }>Save</Button>
                <Button type="default" icon="rollback" onClick={ this.handleBack }>Back</Button>
            </Button.Group>
        ]
        
        return(
            <div>
                { !this.state.inserting &&
                    <Card title="Quizs" extra={extraButton}>
                        <Table rowKey={ item => item.id } dataSource={this.props.quizs} columns={columns} ></Table>
                    </Card>
                }

                { this.state.inserting &&
                    <Card title="New Quiz" actions={ actionInsert }>
                        <Form>
                            <Form.Item label="Description">
                                <Input value={ this.state.description } onChange={ this.handleDescription } />
                            </Form.Item>

                            <Form.Item label="Add questions">
                                <AutoComplete 
                                    filter={ true } 
                                    filterOption={ this.findQuestions }
                                    placeholder="Select question for add"  
                                    allowClear={ true } 
                                    dataSource={ this.state.dataSourceQuestion } 
                                    onSelect={  this.handleSelectQuestion  } />
                            </Form.Item>

                            {
                                this.state.questions.length > 0 &&
                                    <table cellPadding="10">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Question</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            { this.state.questions.map( question => {
                                                return <tr key={question.id}>
                                                            <td>
                                                                <Popconfirm title="Are sure?" onConfirm={()=> this.handleRemoveQuestion(question) }>
                                                                    <Button shape="circle" icon="delete" type="danger"></Button>
                                                                </Popconfirm>
                                                            </td>
                                                            <td>{ question.description }</td>
                                                        </tr>
                                                    
                                            }) }
                                        </tbody>
                                    </table>    
                            }
                            
                        </Form>
                    </Card>
                }
            </div>
        )
    }

    componentDidMount(){
        this.props.loadQuestions();
        this.props.loadQuizs();
    }

    componentWillReceiveProps(nextProps){
        let { questions } = nextProps;
        let dataSourceQuestion = []
        questions.map( question => dataSourceQuestion.push({value: question.id, text: question.description}) )
        this.setState({...this.state, dataSourceQuestion});

    }

    handleSaveQuiz = ()=>{
        this.setState({ ...this.state, saving: true, inserting: false, questions:[] });
        let quiz = {id: uuidv1(), description: this.state.description, questions: this.state.questions };
        this.props.addQuiz( quiz );
        info({
            title: "Success insert",
            content: "Your quiz has be saved!"
        });

    }

    handleDescription = event =>{
        this.setState({...this.state, description: event.target.value});
    }

    handleRemoveQuestion = questionRemove =>{
        let { questions, dataSourceQuestion } = this.state;
        dataSourceQuestion.push({ value: questionRemove.id, text: questionRemove.description })
        questions = questions.filter( question => question.id !== questionRemove.id );

        this.setState({...this.state, dataSourceQuestion, questions})

    }

    handleSelectQuestion = question =>{
        let { questions, dataSourceQuestion } = this.state;
        question = this.props.questions.find( quest => quest.id === question)

        dataSourceQuestion = dataSourceQuestion.filter( quest => quest.value !== question.id );
        questions.push(question);

        this.setState({...this.state, dataSourceQuestion, questions})
    }

    findQuestions = (inputValue, option) =>{
        return option.props.children.toUpperCase().indexOf(inputValue.toUpperCase()) !== -1;
    }

    handleNewQuiz = ()=>{
        let { questions } = this.props;
        let dataSourceQuestion = []
        questions.map( question => dataSourceQuestion.push({value: question.id, text: question.description}) )
        this.setState({ ...this.state, description: null, questions: [], dataSourceQuestion, inserting: true, saving: false })
    }

    handleBack = ()=>{
        this.setState({...this.state, inserting: false});
    }

    handleRemoveQuiz = quiz =>{
        this.props.removeQuiz(quiz);
        notification['success']({
            message: "Quiz removed!",
            description: "The quiz has been successfully removed!"
        })

    }

}

const mapStateToProps = state =>({
    questions: state.questionReducer.questions,
    quizs: state.quizReducer.quizs
});

const mapDispatchToProps = dispatch =>({
    loadQuestions: ()=> dispatch(loadQuestions()),
    addQuiz: quiz => dispatch(addQuiz(quiz)),
    loadQuizs: () => dispatch(loadQuizs()),
    removeQuiz: quiz => dispatch(removeQuiz(quiz))
});

export default connect(mapStateToProps, mapDispatchToProps)(Quiz);

 