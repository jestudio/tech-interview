import React from 'react';
import { Card } from 'antd';

class Home extends React.Component{
    render(){
        return(
            <Card title="Quiz system React + Redux">
                <p>Welcome to my test page.</p>
            </Card>
        )
    }
}

export default Home;

