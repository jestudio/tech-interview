
# Test Jefferson Oliveira

**Install: **

- Open folder frontend
- npm install
- npm start

#Technologies

- React + Redux
- Ant Design
- Storage: Local Store

#Application
1. Create your questions with answers
2. Create your quizs with related to registered questions
3. Response quiz

# Development
In my components I usually use the state and leave the redux to receive my object already mounted.

About the test, I'm still studying test, so I did not apply because it could delay the delivery of the project.

About the comments in the codes, I try to create my code as readable as possible so that there is no need for comments, but in some moments we have to, in the project there are few comments.

I hope the project is within the standard that you expect, thanks for the opportunity.

Att,

Jefferson Oliveira




